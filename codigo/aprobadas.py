#!/usr/bin/env python
# encoding: utf-8

import utilidades

NUEVA_LINEA = "\n"


def leer_archivo(nombre_de_archivo):
    """Devolver una lista con los códigos de las materias aprobadas guardados
    en el archivo <nombre_de_archivo>. Si el archivo no existe crearlo con
    encabezado y devolver una lista vacía.

    @:param nombre_de_archivo (str):
    @:return (list):
    """

    if not utilidades.existe_archivo(nombre_de_archivo):
        try:
            archivo_aprobadas = open(nombre_de_archivo, "w")
            archivo_aprobadas.write("codigo" + NUEVA_LINEA)
            archivo_aprobadas.close()

            archivo_aprobadas = open(nombre_de_archivo, "r")

        except IOError as e:
            raise e("No se pudo crear el archivo de materias aprobadas.")
    else:
        try:
            archivo_aprobadas = open(nombre_de_archivo, "r")
        except IOError as e:
            raise e("No se pudo abrir el archivo de materias  aprobadas.")

    aprobadas = _archivo_a_lista(archivo_aprobadas)
    archivo_aprobadas.close()
    return aprobadas


def _archivo_a_lista(archivo_aprobadas):
    """Devolver una lista con los códigos de las materias aprobadas guardados
    en el archivo <archivo_aprobadas>. Si no hay aprobadas devolver una lista
    vacía.

    :param archivo_aprobadas (file): Archivo abierto, con encabezado,  donde se
    guardan los códigos de las materias aprobadas.
    :return (list):
    """

    archivo_aprobadas.next()  # saltear el encabezado
    resultado = []
    for linea in archivo_aprobadas:
        registro = __obtener_registro(linea)
        resultado.append(registro)
    return resultado

def __obtener_registro(linea):

    return linea.rstrip(NUEVA_LINEA)


def guardar(aprobadas, nombre_de_archivo):

    """

    :param aprobadas: Lista con los códigos de las materias aprobadas.
    :param nombre_de_archivo: Nombre del archivo donde se guardan las materias
    aprobadas (se sobreescribe su contenido).
    :return:
    """

    try:
        archivo_aprobadas = open(nombre_de_archivo, "w")
        # escribir encabezado
        archivo_aprobadas.write("codigo" + NUEVA_LINEA)
        for codigo in aprobadas:
            archivo_aprobadas.write(codigo + NUEVA_LINEA)
    except IOError as e:
        raise e

    archivo_aprobadas.close()

def esta_aprobada(aprobadas, codigo_materia):
    """Devolver True si la materia cuyo código es <codigo_materia> está
    registrada como aprobada; caso contrario devolver False.

    :param aprobadas (list): Lista con los códigos de las materias aprobadas.
    :param codigo_materia (str): Código de la materia que se quiere saber si
    está aprobada.
    """

    resultado = False
    for codigo in aprobadas:
        if codigo == codigo_materia:
            resultado = True
            break

    return resultado




