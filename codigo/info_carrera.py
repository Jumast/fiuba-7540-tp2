#!/usr/bin/env python
# encoding: utf-8

import materias as materias_
import aprobadas as aprobadas_


def tiene_correlativas_aprobadas(materias, aprobadas, codigo_materia):
    """Devolver True si la materia cuyo código es <codigo_materia> tiene
    todas sus correlativas aprobadas; caso contrario devolver False.

    :param materias (list): Lista con la información de todas las materias.
    :param aprobadas: Lista con los códigos de las materias aprobadas.
    :return(bool)

    """

    correlativas = materias_.obtener_correlativas(materias, codigo_materia)
    if not correlativas:
        return True
    else:
        for codigo in correlativas:
            if aprobadas_.esta_aprobada(aprobadas, codigo):
                continue
            else:
                return False
        return True


def obtener_habilitadas(materias, aprobadas):
    """Devolver una lista con los códigos de todas las materias habilitadas
    para cursar. Incluir las materias que ya se hayan aprobado.

    :param materias (list): Lista con la información de todas las materias.
    :param aprobadas: Lista con los códigos de las materias aprobadas.
    :return(list)
    """
    materias_todas = materias_.obtener_codigos(materias)
    materias_habilitadas = []
    for codigo_materia in materias_todas:
        if tiene_correlativas_aprobadas(materias, aprobadas, codigo_materia):
            materias_habilitadas.append(codigo_materia)
    return materias_habilitadas


def obtener_creditos_aprobados(materias, aprobadas):
    """Devolver la cantidad total de créditos aprobados.

    :param materias (list): Lista con la información de todas las materias.
    :param aprobadas: Lista con los códigos de las materias aprobadas.
    """

    creditos_totales = 0
    for codigo_materia in aprobadas:
        creditos = materias_.obtener_creditos(materias, codigo_materia)
        creditos_totales += creditos
    return creditos_totales

