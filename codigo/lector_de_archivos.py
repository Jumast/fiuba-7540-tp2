#!/usr/bin/env python
# encoding: utf-8

import aprobadas as aprobadas_
import materias as materias_


def leer_archivos(nombre_archivo_materias, nombre_archivo_aprobadas):
    """Devolver una tupla con las siguientes dos listas:
     * Lista con la información de cada una de las materias.
     * Lista con los códigos de las materias aprobadas.

    :param nombre_archivo_materias (str): Nombre del archivo donde está
    registrada la información de cada una de las materias de la carrera. El
    archivo debe existir.
    :param nombre_archivo_aprobadas (str): Nombre del archivo donde se
    registran las materias aprobadas. El archivo puede existir o no.
    :return (tuple):
    """

    try:
        info_materias = materias_.leer_archivo(nombre_archivo_materias)
    except:
        mensaje = "No se pudo leer el archivo {}".format(nombre_archivo_materias)
        raise Exception(mensaje)

    try:
        aprobadas = aprobadas_.leer_archivo(nombre_archivo_aprobadas)
    except:
        mensaje = "No se pudo leer el archivo {}".format(nombre_archivo_aprobadas)
        raise Exception(mensaje)

    return info_materias, aprobadas



