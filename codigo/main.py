#!/usr/bin/env python
# encoding: utf-8

import lector_de_archivos
import menu_principal
import mensajes

ARCHIVO_MATERIAS = "materias.csv"
ARCHIVO_APROBADAS = "aprobadas.csv"

def main():
    """Punto de entrada del programa.
    Leer los archivos de materias y aprobadas, mostrar el menú principal con
    las opciones y procesar la opción elegida por el usuario.

    El archivo de materias debe existir. El de aprobadas puede no existir; si
    no existe crearlo. Mostrar el menú principal repetidamente hasta que el
    usuario cancele la interacción.

    """

    try:
        materias, aprobadas = lector_de_archivos.leer_archivos(ARCHIVO_MATERIAS, ARCHIVO_APROBADAS)
    except Exception as e:
        print e.message
        mensajes.infomar_fin_anticipado_del_programa()
        return

    mensajes.dar_la_bienvenida_al_sistema()

    continuar = True
    while continuar:

        opcion_menu_principal = menu_principal.obtener_opcion()
        try:
            continuar = menu_principal.procesar_opcion(opcion_menu_principal, materias, aprobadas, ARCHIVO_APROBADAS)
        except IOError as e:
            print e.message
            mensajes.infomar_fin_anticipado_del_programa()
            return

main()


