#!/usr/bin/env python
# encoding: utf-8

import csv
import utilidades

CODIGO = 0
NOMBRE = 1
CREDITOS = 2
CORRELATIVAS = 3


def leer_archivo(nombre_de_archivo):
    """Devolver una lista con la información de las materias guardada en el
    archivo <nombre_de_archivo>.

    :param nombre_de_archivo (str):
    :return (list):
    """

    if not utilidades.existe_archivo(nombre_de_archivo):
        mensaje = ("No se pudo abrir el archivo '{}' porque el "
                   "archivo no existe.".format(nombre_de_archivo))
        raise IOError(mensaje)

    # el archivo de materias existe
    try:
        archivo_materias = open(nombre_de_archivo)
    except IOError as e:
        mensaje = "No se pudo abrir el archivo '{}'".format(nombre_de_archivo)
        raise e(mensaje)

    materias = _archivo_a_lista(archivo_materias)
    archivo_materias.close()
    return materias


def _archivo_a_lista(archivo_materias):
    """Devolver una lista con la información de las materias guardada en el
    archivo <archivo_materias>.

    :param archivo_materias (file): Arhivo abierto donde se guarda la
    información de las materias. El archivo tiene encabezado y sus líneas
    son como se muestra a continuación:
    61.09,Probabilidad y Estadistica B,6,61.03-61.08

    :return (list):
    """

    archivo_materias.next()  # no leer el encabezado
    materias_csv = csv.reader(archivo_materias)
    resultado = []
    for registro in materias_csv:
        codigo = registro[CODIGO]
        nombre = registro[NOMBRE]
        creditos = (int)(registro[CREDITOS])
        correlativas = registro[CORRELATIVAS]
        resultado.append([codigo, nombre, creditos, correlativas])
    return resultado

def obtener_codigos(materias):
    """Devolver una lista con los códigos de todas las materias.

    :param materias (list): Lista con la información de todas las materias.

    Ejemplo:

    >> materias = [
        "75.03","Organizacion del Computador","8","75.40","",
        "75.41","Algoritmos y Programacion II","6","75.40","",
        "61.09","Probabilidad y Estadistica B","6","61.03-61.08"
    ]

    >> resultado = obtener_codigos(materias)
    >> resultado
    >> [75.03, 75.41, 61.09]
    """

    resultado = []
    for materia in materias:
        codigo = materia[CODIGO]
        resultado.append(codigo)

    return resultado


def existe(materias, codigo_materia):
    """Devolver True si existe la materia cuyo código es <codigo_materia>;
    caso contrario devolver False.

    :param materias (list): Lista con la información de todas las materias.
    :param codigo_materia (str):

    Ejemplo:

    >> materias = [
        "75.03","Organizacion del Computador","8","75.40","",
        "75.41","Algoritmos y Programacion II","6","75.40","",
        "61.09","Probabilidad y Estadistica B","6","61.03-61.08"
    ]

    >> existe_materia = existe(materias, "75.41")
    >> existe_materia
    >> True

    """

    for materia in materias:

        existe_materia = False
        codigo = materia[CODIGO]
        if codigo == codigo_materia:
            existe_materia = True
            break

    return existe_materia


def obtener_nombre(materias, codigo_materia):
    """Devolver el nombre de la materia cuyo código es <codigo_materia>.

    :param materias (list): Lista con la información de todas las materias.
    :param codigo_materia (str): Código de la materia cuyo nombre se quiere
    averiguar. Debe ser un código válido, es decir, un código de materia que
    figure en <materias>.

    Ejemplo:

    >> materias = [
        "75.03","Organizacion del Computador","8","75.40","",
        "75.41","Algoritmos y Programacion II","6","75.40","",
        "61.09","Probabilidad y Estadistica B","6","61.03-61.08"
    ]

    >> codigo_materia = 75.03
    >> nombre_materia = obtener_nombre(materias, codigo_materia)
    >> nombre_materia = "Organizacion del Computador"
    """

    for materia in materias:

        codigo = materia[CODIGO]
        if codigo == codigo_materia:
            nombre_materia = materia[NOMBRE]
            break

    return nombre_materia


def obtener_creditos(materias, codigo_materia):
    """Devolver lo créditos de la materia cuyo código es <codigo_materia>.

    :param materias (list): Lista con la información de todas las materias.
    :param codigo_materia (str): Código de la materia cuya cantidad de creditos
    se quiere averiguar. Debe ser un código válido, es decir, un código de
    materia que  figure en <materias>.

    Ejemplo:

    >> materias = [
        "75.03","Organizacion del Computador","8","75.40","",
        "75.41","Algoritmos y Programacion II","6","75.40","",
        "61.09","Probabilidad y Estadistica B","6","61.03-61.08"
    ]

    >> creditos = obtener_creditos(materias, "61.09")
    >> creditos
    >> 6
    """

    for materia in materias:

        codigo = materia[CODIGO]
        if codigo == codigo_materia:
            creditos = materia[CREDITOS]
            break

    return creditos


def obtener_correlativas(materias, codigo_materia):
    """Devolver una lista con los códigos de las correlativas de la materia
    cuyo código es <código_materia>. Si no tiene correlativas, devolver una
    lista vacía.

    :param materias (list): Lista con la información de todas las materias.
    :param codigo_materia (str): Código de la materia cuya cantidad de creditos
    se quiere averiguar. Debe ser un código válido, es decir, un código de
    materia que  figure en <materias>.

    Ejemplo:

    >> materias = [
        "61.03","Analisis Matematico II A","8",""
        "75.03","Organizacion del Computador","8","75.40","",
        "75.41","Algoritmos y Programacion II","6","75.40","",
        "61.09","Probabilidad y Estadistica B","6","61.03-61.08"
    ]

    >> correlativas = obtener_correlativas(materias, "61.09")
    >> correlativas
    >> ["61.03","61.08"]

    >> correlativas = obtener_correlativas(materias, "61.03")
    >> correlativas
    >> []

    """

    for materia in materias:

        codigo = materia[CODIGO]
        if codigo == codigo_materia:
            registro_correlativas = materia[CORRELATIVAS]
            posibles_correlativas = registro_correlativas.split("-")

    return posibles_correlativas if not posibles_correlativas == [""] else []



