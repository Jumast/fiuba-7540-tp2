#!/usr/bin/env python
# encoding: utf-8


def informar_no_existe_archivo(nombre_de_archivo):
    """Mostrar por consola un mensaje indicando que no existe el archivo
    nombre_de_archivo."""

    mensaje = u"No se encontró el archivo"
    mensaje += " "
    mensaje += "'" + nombre_de_archivo + "'"
    mensaje += "."
    print mensaje


def infomar_fin_anticipado_del_programa():
    """Mostrar por consola un mensaje avisando que el programa no puede
    continuar"""

    print u"El programa no puede continuar."


def dar_la_bienvenida_al_sistema():
    """Mostrar por consola un mensaje dando la bienvenida al sistema."""

    print
    print u"Bienvenido al sistema de gestión de materias!"
    print


def infomar_error_de_lectura_de_archivo(nombre_de_archivo):
    """Mostrar por consola un mensaje informando que se produjo un error al
    intentar leer el archivo nombre_de_archivo"""

    print "Error al intentar leer el archivo {}.".format(nombre_de_archivo)


def informar_error_creacion_de_archivo(nombre_de_archivo):
    """Mostrar por consola un mensaje informando que se produjo un error al
    intentar crear el archivo nombre_de_archivo"""

    print "Error al intentar crear el archivo {}.".format(nombre_de_archivo)
