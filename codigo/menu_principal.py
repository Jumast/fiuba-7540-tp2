#!/usr/bin/env python
# encoding: utf-8

import opcion_marcar_aprobada
import opcion_ver_aprobadas
import opcion_ver_habilitadas
import opcion_informacion_detallada
import aprobadas as aprobadas_

OPCION_MARCAR_APROBADA = 1
OPCION_VER_APROBADAS = 2
OPCION_VER_HABILITADAS = 3
OPCION_INFORMACION_DETALLADA = 4
OPCION_SALIR = 5
OPCIONES = [
    OPCION_MARCAR_APROBADA,
    OPCION_VER_APROBADAS,
    OPCION_VER_HABILITADAS,
    OPCION_INFORMACION_DETALLADA,
    OPCION_SALIR
]


def obtener_opcion():
    """Mostrar al usuario el menú principal y solictar una opción. Devolver la
    opción ingresada.

    Presentar las siguientes opciones:
     * Marcar una materia como aprobada.
     * Ver las materias aprobadas y la cantidad de créditos
     * Ver las materias habilitadas para cursar.
     * Ver información detallada de una materia.
     * Salir.

    :return (int):
    """

    while True:

        _mostrar_menu_principal()
        ingreso_usuario = raw_input()

        if _es_ingreso_valido_para_opcion_del_menu(ingreso_usuario):
            opcion = _convertir_ingreso_usuario_a_opcion(ingreso_usuario)
            return opcion
        else:
            print u"La opción ingresada es inválida."
            print

def _mostrar_menu_principal():
    """Mostrar el menú principal.

    Ejemplo:

    MENU PRINCIPAL
    ==============

    ¿Qué desea hacer?

    1. Marcar una materia como aprobada.
    2. Ver las materias aprobadas y la cantidad de créditos.
    3. Ver las materias habilitadas para cursar.
    4. Ver la información detallada de una materia.
    5. Salir y guardar.

    Ingrese una opción: [1, 2, 3, 4, 5]
    """

    print
    mensaje = "MENU PRINCIPAL"
    print mensaje
    print "".join(["=" for c in mensaje])
    print

    print u"¿Qué desea hacer?"
    print
    print u"{}. Marcar una materia como aprobada.".format(OPCION_MARCAR_APROBADA)
    print u"{}. Ver las materias aprobadas y la cantidad de créditos.".format(OPCION_VER_APROBADAS)
    print u"{}. Ver las materias habilitadas para cursar.".format(OPCION_VER_HABILITADAS)
    print u"{}. Ver la información detallada de una materia.".format(OPCION_INFORMACION_DETALLADA)
    print u"{}. Salir y guardar.".format(OPCION_SALIR)
    print
    print u"Ingrese una opción: {}".format(OPCIONES)
    print ">>"


def _es_ingreso_valido_para_opcion_del_menu(ingreso_usuario):
    """Devolver True si <ingreso_usuario> corresponde a una
    de las opciones ofrecidas en en menú. Devolver False si no.

    :param ingreso_usuario (str): Valor ingresado por el usuario cuando se le
    presenta el menú principal.
    :return (bool):
    """

    return ingreso_usuario.isdigit() and int(ingreso_usuario) in OPCIONES

def _convertir_ingreso_usuario_a_opcion(ingreso_usuario):
    """Convertir la cadena ingresada por el usuario al entero correspondiente
    a la opción. <ingreso_usuario> debe corresponder a una opción valida.

    :param ingreso_usuario (str):
    :return (int):
    """

    opcion = int(ingreso_usuario)
    assert opcion in OPCIONES
    return opcion

def procesar_opcion(opcion, materias, aprobadas, nombre_archivo_aprobadas):
    """Procesar la opción del menú principal elegida por el usuario.

    :param opcion: Opción del menú principal elegida por el usuario.
    :param materias: Lista con la información de cada materia.
    :param aprobadas Lista con las materias aprobadas:
    :param nombre_archivo_aprobadas: Nombre del archivo donde se guardan las
    materias aprobadas.
    :return (None):
    """

    if opcion == OPCION_SALIR:

        try:
            aprobadas_.guardar(aprobadas, nombre_archivo_aprobadas)
        except IOError as e:
            raise e

        print(u"Gracias por utilizar el sistema de gestión de materias.")
        return False

    elif opcion == OPCION_MARCAR_APROBADA:
        opcion_marcar_aprobada.iniciar_interaccion(materias, aprobadas)
        return True

    elif opcion == OPCION_VER_APROBADAS:
        opcion_ver_aprobadas.mostrar_aprobadas(materias, aprobadas)
        return True

    elif opcion == OPCION_VER_HABILITADAS:
        opcion_ver_habilitadas.mostrar_habilitadas(materias, aprobadas)
        return True

    elif opcion == OPCION_INFORMACION_DETALLADA:
        opcion_informacion_detallada.iniciar_interaccion(materias, aprobadas)
        return True

    else:
        raise

