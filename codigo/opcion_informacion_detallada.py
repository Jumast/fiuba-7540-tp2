#!/usr/bin/env python
# encoding: utf-8

import interaccion
import materias as materias_

SENTINELA = "*"
LONG_CODIGO = 5
LONG_NOMBRE = 55
LONG_CREDITOS = 3
CORRELATIVA_CBC = "CBC"


def iniciar_interaccion(materias, aprobadas):

    print
    mensaje = u"Información detallada."
    print mensaje
    print "".join(["-" for c in mensaje])
    print

    while True:

        ingreso_usuario = __pedir_codigo_materia()

        if ingreso_usuario == SENTINELA:
            return interaccion.cancelar()
        else:
            posible_codigo_materia = ingreso_usuario
            if materias_.existe(materias, posible_codigo_materia):
                codigo = posible_codigo_materia
                nombre = materias_.obtener_nombre(materias, codigo)
                creditos = materias_.obtener_creditos(materias, codigo)
                aprobada = ("sí" if codigo in aprobadas else "no")
                correlativas = materias_.obtener_correlativas(materias, codigo)

                print u"" + codigo + " - " + nombre
                print (str(creditos)).decode("UTF-8") + " " + u"créditos"
                print u"" + "Aprobada: " + aprobada.decode("UTF-8")
                print

                print "Correlativas:"
                if not correlativas:
                    print CORRELATIVA_CBC
                    break
                else:
                    for codigo_materia in correlativas:
                        nombre = materias_.obtener_nombre(materias, codigo_materia)
                        aprobada = codigo_materia in aprobadas
                        creditos = materias_.obtener_creditos(materias, codigo_materia)
                        __mostrar_info_correlativa(codigo_materia, nombre,
                                                   creditos, aprobada)
                    break
            else:
                print u"El código ingresado en incorrecto."


def __pedir_codigo_materia():
    """Solicitar al usuario el código de la materia que se quiere marcar
    como aprobada."""

    print (u"Ingrese el código de la materia o presione {} para cancelar: ".format(SENTINELA))
    print ">>"
    ingreso_usuario = raw_input()
    return ingreso_usuario


def __mostrar_info_correlativa(codigo, nombre, creditos, aprobada):

    mensaje = (codigo + " - " + nombre).ljust(LONG_CODIGO + LONG_NOMBRE)
    mensaje += " "
    mensaje += (str(creditos) + "c").ljust(LONG_CREDITOS + 1)  # 1 == len("c")
    msj_aprobada = "aprobada" if aprobada else ""
    mensaje += msj_aprobada
    print mensaje
