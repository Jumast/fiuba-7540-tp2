#!/usr/bin/env python
# encoding: utf-8

import interaccion
import materias as materias_
import aprobadas as aprobadas_
import info_carrera

SENTINELA = "*"


def iniciar_interaccion(materias, aprobadas):

    print
    mensaje = "Marcar materia como aprobada."
    print mensaje
    print "".join(["-" for c in mensaje])
    print

    while True:

        ingreso_usuario = __pedir_codigo_materia()

        if ingreso_usuario == SENTINELA:
            return interaccion.cancelar()
        else:
            posible_codigo_materia = ingreso_usuario
            if materias_.existe(materias, posible_codigo_materia):
                codigo = posible_codigo_materia
                nombre = materias_.obtener_nombre(materias, codigo)
                if aprobadas_.esta_aprobada(aprobadas, codigo):
                    __informar_materia_ya_registrada(codigo, nombre)
                elif not info_carrera.tiene_correlativas_aprobadas(materias, aprobadas, codigo):
                    __infomar_correlativas_no_aprobadas(codigo, nombre)
                else:
                    confirmacion = __obtener_confirmacion(codigo, nombre)
                    if (confirmacion):
                        aprobadas.append(codigo)
                        __informar_registrada(codigo, nombre)
                        break
                    # else:
                        # return interaccion.cancelar()

            else:
                print "El codigo ingresado en incorrecto."


def __pedir_codigo_materia():
    """Solicitar al usuario el código de la materia que se quiere marcar
    como aprobada."""

    print (u"Ingrese el código de la materia o presione {} para cancelar: ".format(SENTINELA))
    print ">>"
    ingreso_usuario = raw_input()
    return ingreso_usuario


def __informar_materia_ya_registrada(codigo_materia, nombre_materia):
    """Informar al usuario que la materia ya está registrada como aprobada."""

    print codigo_materia + " - " + nombre_materia
    print "La materia ya se encuentra registrada como aprobada."


def __infomar_correlativas_no_aprobadas(codigo_materia, nombre_materia):
    """Infomar al usuario que no se puede registrar la materia como aprobada
    porque no se encuentran registradas como aprobadas sus correlativas."""

    print codigo_materia + " - " + nombre_materia
    print "No se puede registrar como aprobada. No tiene sus correlativas " \
          "aprobadas."


def __informar_registrada(codigo, nombre):
    """Informar al usuario que la materia se regitró como aprobada
    exitosamente."""

    print u"Se registró {} ({}) como materia aprobada.".format(nombre, codigo)


def __obtener_confirmacion(codigo_materia, nombre_materia):
    """Pedirle al susuario que confirme si quiere registrar la materia como
    aprobada. En caso afirmativo devolver True; caso contrario devolver False.
    """

    while True:
        print codigo_materia + " - " + nombre_materia
        print u"¿Marcar materia como aprobada? [s/n]"
        print ">>"
        ingreso_usuario = raw_input()

        if ingreso_usuario.lower() == "s":
            return True
        elif ingreso_usuario.lower() == "n":
            return False
        else:
            print u"Opcion no válida."
            print
