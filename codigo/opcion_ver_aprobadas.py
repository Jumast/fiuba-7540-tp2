#!/usr/bin/env python
# encoding: utf-8

import materias as materias_
import info_carrera


LONG_CODIGO = 5
LONG_NOMBRE = 55
LONG_CREDITOS = 3


def mostrar_aprobadas(materias, aprobadas):
    """Mostrar el código, nombre y créditos de todas las materais que fueron
    marcadas como aprobadas, así como también el total de créditos.

    Ej:

    Materias aprobadas.
    ===================

    61.03 - Analisis Matematico II A                             8c
    61.08 - Algebra II A                                         8c
    75.40 - Algoritmos y Programacion I                          6c
    ---------------------------------------------------------------
    Total                                                       22c
    """

    print
    mensaje = "Materias aprobadas."
    print mensaje
    print "".join(["-" for c in mensaje])
    print

    if not aprobadas:
        print "No tiene materias aprobadas."
    else:
        for codigo_materia in aprobadas:
            creditos = materias_.obtener_creditos(materias, codigo_materia)
            nombre = materias_.obtener_nombre(materias, codigo_materia)
            __mostrar_info_materia_aprobada(codigo_materia, nombre, creditos)
        __mostrar_total_creditos(info_carrera.obtener_creditos_aprobados(materias, aprobadas))


def __mostrar_info_materia_aprobada(codigo, nombre, creditos):

    mensaje = (codigo + " - " + nombre).ljust(LONG_CODIGO + LONG_NOMBRE)
    mensaje += " "
    mensaje += (str(creditos) + "c").ljust(LONG_CREDITOS + 1)  # 1 == len("c")
    print mensaje


def __mostrar_total_creditos(creditos):

    # len("Total") == LONG_CODIGO == 5
    valor_ljust = LONG_CODIGO + LONG_NOMBRE
    mensaje = "Total".ljust(valor_ljust) + str(creditos).ljust(2) + "c"
    print "".join(["-" for c in mensaje])
    print mensaje

