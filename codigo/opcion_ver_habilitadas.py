#!/usr/bin/env python
# encoding: utf-8

import materias as materias_
import info_carrera

LONG_CODIGO = 5
LONG_NOMBRE = 55
LONG_CREDITOS = 3


def mostrar_habilitadas(materias, aprobadas):
    """Devolver una lista con los códigos de todas las materias habilitadas
    para cursar. No incluir las materias que ya se hayan aprobado.

    """

    print
    mensaje = "Materias habilitadas para cursar."
    print mensaje
    print "".join(["-" for c in mensaje])
    print

    habilitadas = info_carrera.obtener_habilitadas(materias, aprobadas)
    for codigo_materia in habilitadas:
        if not (codigo_materia in aprobadas):
            creditos = materias_.obtener_creditos(materias, codigo_materia)
            nombre = materias_.obtener_nombre(materias, codigo_materia)
            __mostrar_info_materia_aprobada(codigo_materia, nombre, creditos)


def __mostrar_info_materia_aprobada(codigo, nombre, creditos):

    mensaje = (codigo + " - " + nombre).ljust(LONG_CODIGO + LONG_NOMBRE)
    mensaje += " "
    mensaje += (str(creditos) + "c").ljust(LONG_CREDITOS + 1)  # 1 == len("c")
    print mensaje