#!/usr/bin/env python
# encoding: utf-8

import os

def existe_archivo(nombre_de_archivo):
    """Devolver True si existe el archivo nombre_de_archivo. Devolver False si
    no existe.

    :param nombre_de_archivo (str):
    :return (bool):
    """

    return os.path.exists(nombre_de_archivo) and os.path.isfile(nombre_de_archivo)

